ACT_DE_MODEL    模型, 没有发布的模板
ACT_DE_MODEL_HISTORY    ACT_DE_MODEL 的历史模板
ACT_GE_BYTEARRAY    发布的模板, 每个模板包含.bpmn文件, 与封面图片
ACT_RE_PROCDEF  发布版本, key唯一,
ACT_RE_DEPLOYMENT   每次发布的模板信息
ACT_RU_ACTINST  运行的流程经过的节点信息
ACT_RU_EXECUTION    执行中的节点 (执行实例表)
ACT_RU_TASK     当前执行的节点任务


ACT_HI_ACTINST  运行历史节点
ACT_HI_PROCINST 运行历史流程
ACT_HI_TASKINST 历史任务
ACT_HI_VARINST  历史变量