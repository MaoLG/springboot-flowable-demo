package com.example.springbootflowabledemo.entrust;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Slf4j
@Component(value = "net_operatorGetter")
public class NetOperatorGetter implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
//        log.info("net_operatorGetter: 委托表达式, 将路线修改为调度");
//        execution.setVariable("case2", "ATTEMPER");
        log.info("委托表达式处理 execution={}", JSON.toJSONString(execution));
    }
}
