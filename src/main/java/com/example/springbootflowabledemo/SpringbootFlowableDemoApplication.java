package com.example.springbootflowabledemo;

import com.example.springbootflowabledemo.util.SpringUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SpringbootFlowableDemoApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext app = SpringApplication.run(SpringbootFlowableDemoApplication.class, args);
		SpringUtil.setAppContext(app);
	}

}
