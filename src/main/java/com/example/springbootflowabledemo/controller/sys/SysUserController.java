package com.example.springbootflowabledemo.controller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.springbootflowabledemo.domian.R;
import com.example.springbootflowabledemo.domian.pojo.sys.SysUser;
import com.example.springbootflowabledemo.domian.req.UserDistributeDeptReq;
import com.example.springbootflowabledemo.service.sys.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sysUser")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    /**
     * 新增用户
     *
     * @param sysUser
     * @return
     */
    @PostMapping("")
    public R addUser(@RequestBody SysUser sysUser) {
        return R.success(sysUserService.addUser(sysUser));
    }


    /**
     * 删除用户
     *
     * @param ids
     * @return
     */
    @DeleteMapping("")
    public R deleteUser(Long[] ids) {
        return R.success(sysUserService.deleteUser(ids));
    }

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    @GetMapping("")
    public R listUser(Page<SysUser> page) {
        return R.success(sysUserService.listUser(page));
    }

    /**
     * 用户分配部门
     *
     * @param req
     * @return
     */
    @PostMapping("/userDept")
    public R userDistributeDept(@RequestBody UserDistributeDeptReq req) {
        sysUserService.userDistributeDept(req);
        return R.success();
    }
}
