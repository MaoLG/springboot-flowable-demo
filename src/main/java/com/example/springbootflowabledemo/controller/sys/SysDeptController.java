package com.example.springbootflowabledemo.controller.sys;

import com.example.springbootflowabledemo.domian.R;
import com.example.springbootflowabledemo.domian.pojo.sys.SysDept;
import com.example.springbootflowabledemo.service.sys.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sysDept")
public class SysDeptController {
    @Autowired
    private SysDeptService sysDeptService;

    /**
     * 新增部门
     *
     * @param sysDept
     * @return
     */
    @PostMapping("")
    public R addDept(@RequestBody SysDept sysDept) {
        sysDeptService.addDept(sysDept);
        return R.success();
    }


    /**
     * 删除部门
     *
     * @param id
     * @return
     */
    @DeleteMapping("")
    public R deleteDept(Long id) {
        sysDeptService.deleteDept(id);
        return R.success();
    }

    /**
     * 查询树结构部门信息
     * @param parentId
     * @return
     */
    @GetMapping("")
    public R treeList(Long parentId){
        return R.success(sysDeptService.getDeptByParentId(parentId));
    }


    /**
     * 查询部门以及下的用户
     * @param parentId
     * @return
     */
    @GetMapping("/deptWithUser")
    public R deptWithUser(Long parentId){
        return R.success(sysDeptService.getDeptWithUser(parentId));
    }
}
