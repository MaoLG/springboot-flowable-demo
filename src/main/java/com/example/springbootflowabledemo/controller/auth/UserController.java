package com.example.springbootflowabledemo.controller.auth;

import com.example.springbootflowabledemo.domian.R;
import com.example.springbootflowabledemo.domian.req.UserLoginReq;
import com.example.springbootflowabledemo.service.sys.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class UserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 用户登录
     *
     * @param req
     * @return
     */
    @PostMapping("/login")
    public R login(@RequestBody UserLoginReq req) {
        return R.success(sysUserService.login(req));
    }
}
