package com.example.springbootflowabledemo.controller;

import com.example.springbootflowabledemo.domian.R;
import com.example.springbootflowabledemo.domian.req.ActivateReq;
import com.example.springbootflowabledemo.domian.req.CompleteReq;
import com.example.springbootflowabledemo.domian.resp.MyProcessResp;
import com.example.springbootflowabledemo.service.FlowableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/flowable-demo")
public class FlowableController {

    @Autowired
    private FlowableService flowableService;

    /**
     * 流程部署
     *
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/deploy")
    public String deploy(MultipartFile file) throws Exception {
        flowableService.deployment(file.getOriginalFilename(), file.getInputStream());
        return "流程部署成功!";
    }

    /**
     * 流程启动
     *
     * @param req
     * @return
     */
    @PostMapping("/activate")
    public R activate(@RequestBody ActivateReq req) {
        flowableService.activate(req.getProcdefId(), req.getTitle(), req.getVariables());
        return R.success("流程启动成功!");
    }

    /**
     * 节点完成
     *
     * @param req
     * @return
     */
    @PostMapping("/complete")
    public R complete(@RequestBody CompleteReq req) {
        flowableService.complete(req.getTaskId(), req.getVariables());
        return R.success("节点处理完成!");
    }

    /**
     * 删除流程定义相关所有数据
     *
     * @param deployId
     * @return
     */
    @PostMapping("/delete/{deployId}")
    public R deleteDeploy(@PathVariable("deployId") String deployId) {
        flowableService.deleteDeploy(deployId);

        return R.success("流程定义删除成功!");
    }

    /**
     * 激活/挂起流程定义
     *
     * @param procdefId test:1:15bac5c7-1dd3-11ed-a283-3cf8620b8a2a
     * @param state     1:激活, 2:挂起
     * @return
     */
    @PostMapping("/deploy/{procdefId}/{state}")
    public R updateDeployState(@PathVariable("procdefId") String procdefId, @PathVariable("state") Integer state) {
        String msg = flowableService.updateDeployState(procdefId, state);
        return R.success(msg);
    }


    /**
     * 激活/挂起流程实例
     *
     * @param processInstanceId e54f94bf-1dd9-11ed-82ff-3cf8620b8a2a
     * @param state             1:激活, 2:挂起
     * @return
     */
    @PostMapping("/process/{processInstanceId}/{state}")
    public R updateProcessState(@PathVariable("processInstanceId") String processInstanceId, @PathVariable("state") Integer state) {
        String msg = flowableService.updateProcessState(processInstanceId, state);
        return R.success(msg);
    }

    /**
     * 获取我启动的发起的流程
     */
    @GetMapping("/process/start")
    public R myStartProcess() {
        List<MyProcessResp> resps = flowableService.myStartProcess();
        return R.success(resps);
    }

    /**
     * 查看我的代办
     *
     * @return
     */
    @GetMapping("/process/todoList")
    public R myTodoListProcess(Integer pageNum, Integer pageSize) {
        return flowableService.myTodoList(pageNum, pageSize);
    }

    @PostMapping("/uiDeployTemplate")
    public R uiDeployTemplate(String processModelId, String processModelHistoryId) {
        flowableService.importTemplate(processModelId, processModelHistoryId);
        return R.success();
    }


    /**
     * 终止流程
     *
     * @param processInstanceId
     * @return
     */
    @PostMapping("/stop")
    public R stopProcess(String processInstanceId) {
        flowableService.stopProcessInstance(processInstanceId);
        return R.success();
    }

}
