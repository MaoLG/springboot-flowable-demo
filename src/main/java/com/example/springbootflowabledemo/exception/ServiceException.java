package com.example.springbootflowabledemo.exception;

import lombok.Data;

@Data
public class ServiceException extends RuntimeException {
    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误描述
     */
    private String ErrorDescribe;

    public ServiceException(String errorDescribe) {
        super();
        this.ErrorDescribe = errorDescribe;
    }


    public ServiceException(String errorCode, String errorDescribe) {
        super();
        this.errorCode = errorCode;
        this.ErrorDescribe = errorDescribe;
    }

}
