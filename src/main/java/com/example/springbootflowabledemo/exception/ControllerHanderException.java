package com.example.springbootflowabledemo.exception;

import com.example.springbootflowabledemo.domian.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class ControllerHanderException {
    /**
     * 业务异常处理
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    //在这个方法里定义我们需要返回的格式
    public R handleUserNotExistException(ServiceException ex) {
        String message = ex.getErrorDescribe();
        return R.error(message);
    }


    /**
     * 全局业务处理
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    //在这个方法里定义我们需要返回的格式
    public R handleExistException(Exception ex) {
        log.error("", ex);
        return R.error("系统内部异常");
    }

}
