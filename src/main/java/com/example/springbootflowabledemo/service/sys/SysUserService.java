package com.example.springbootflowabledemo.service.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.springbootflowabledemo.domian.pojo.sys.SysUser;
import com.example.springbootflowabledemo.domian.req.UserDistributeDeptReq;
import com.example.springbootflowabledemo.domian.req.UserLoginReq;

import java.util.Map;

public interface SysUserService extends IService<SysUser> {

    /**
     * 新增用户
     *
     * @param user
     */
    Boolean addUser(SysUser user);

    /**
     * 删除用户
     *
     * @param ids
     * @return
     */
    Boolean deleteUser(Long[] ids);

    /**
     * 分页查询用户
     *
     * @param page
     * @return
     */
    Page<SysUser> listUser(Page<SysUser> page);

    /**
     * 用户绑定部门
     *
     * @param req
     */
    void userDistributeDept(UserDistributeDeptReq req);

    /**
     * 用户登录
     *
     * @param req
     * @return
     */
    Map login(UserLoginReq req);

    /**
     * 根据用户id查询用户信息
     * @param id
     * @return
     */
    SysUser getUserById(String id);
}
