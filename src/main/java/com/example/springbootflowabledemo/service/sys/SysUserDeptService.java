package com.example.springbootflowabledemo.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.springbootflowabledemo.domian.pojo.sys.SysUserDept;

public interface SysUserDeptService extends IService<SysUserDept> {

}
