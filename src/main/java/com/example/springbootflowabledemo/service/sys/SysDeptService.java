package com.example.springbootflowabledemo.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.springbootflowabledemo.domian.pojo.sys.SysDept;
import com.example.springbootflowabledemo.domian.vo.DeptVo;

import java.util.List;

public interface SysDeptService extends IService<SysDept> {

    /**
     * 新增部门
     *
     * @param sysDept
     */
    void addDept(SysDept sysDept);

    /**
     * 删除部门
     *
     * @param id
     */
    void deleteDept(Long id);


    /**
     * 根据父ID查询部门
     *
     * @param parentId
     * @return
     */
    List<SysDept> getDeptByParentId(Long parentId);

    /**
     * 获取部门与下面的用户
     *
     * @param parentId
     * @return
     */
    List<DeptVo> getDeptWithUser(Long parentId);
}
