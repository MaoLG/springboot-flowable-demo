package com.example.springbootflowabledemo.service.impl.sys;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.springbootflowabledemo.domian.pojo.sys.SysUserDept;
import com.example.springbootflowabledemo.mapper.sys.SysUserDeptMapper;
import com.example.springbootflowabledemo.service.sys.SysUserDeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@DS("sys")
@Service
@Transactional(rollbackFor = Exception.class)
public class SysUserDeptServiceImpl extends ServiceImpl<SysUserDeptMapper, SysUserDept> implements SysUserDeptService {

}
