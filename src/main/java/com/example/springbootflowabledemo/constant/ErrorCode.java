package com.example.springbootflowabledemo.constant;

public interface ErrorCode {

    String ERROR_CODE = "00001";

    String TOKEN_ERROR_CODE = "10001";

    String ERROR_MSG = "呀! 服务器开小差了(*╹▽╹*)!";

}
