package com.example.springbootflowabledemo.domian.resp;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页返回对象
 */
@Data
public class PageResp {

    private Long total;

    private List list = new ArrayList<>();
}
