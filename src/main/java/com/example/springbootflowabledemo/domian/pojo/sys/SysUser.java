package com.example.springbootflowabledemo.domian.pojo.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sys.sys_user")  //多数据源时 最好指定库名
public class SysUser {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String nickname;

    private String username;

    private String pwd;

}
