package com.example.springbootflowabledemo.domian.dto;

import lombok.Data;

/**
 * 代办
 */
@Data
public class TodoListDto {
    private String taskId;

    private String processName;

    private String msg;
}
