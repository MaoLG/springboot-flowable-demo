package com.example.springbootflowabledemo.domian.req;

import lombok.Data;

import java.util.Map;

@Data
public class ActivateReq {
    private String procdefId;
    private String title;
    private Map<String, Object> variables;
}
