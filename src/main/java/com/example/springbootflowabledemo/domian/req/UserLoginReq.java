package com.example.springbootflowabledemo.domian.req;

import lombok.Data;

@Data
public class UserLoginReq {
    private String username;

    private String pwd;
}
