package com.example.springbootflowabledemo.domian.req;

import lombok.Data;

@Data
public class UserDistributeDeptReq {
    private Long userId;

    private Long deptId;
}
