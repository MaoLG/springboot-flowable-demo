package com.example.springbootflowabledemo.domian.req;

import lombok.Data;

import java.util.Map;

@Data
public class CompleteReq {
    private String taskId;
    private Map<String, Object> variables;
}
