package com.example.springbootflowabledemo.domian.vo;

import lombok.Data;

@Data
public class UserVo {
    private Long userId;
    private String nickname;
    private String username;
}
