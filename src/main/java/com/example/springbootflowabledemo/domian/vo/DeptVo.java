package com.example.springbootflowabledemo.domian.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DeptVo {
    private Long deptId;
    private String deptName;

    private List<UserVo> users;
}
