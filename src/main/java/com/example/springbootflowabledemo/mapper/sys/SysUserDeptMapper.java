package com.example.springbootflowabledemo.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springbootflowabledemo.domian.pojo.sys.SysUserDept;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserDeptMapper extends BaseMapper<SysUserDept> {
}
