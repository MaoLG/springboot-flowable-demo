package com.example.springbootflowabledemo.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springbootflowabledemo.domian.pojo.sys.SysDept;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {
}
