package com.example.springbootflowabledemo.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springbootflowabledemo.domian.pojo.sys.SysUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
}
